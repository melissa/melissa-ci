# LXD Images for Spack Containers

This directory contains LXD images for containers with all Spack dependencies pre-installed. Some Melissa dependencies are also installed to reduce Spack built times.

The images can be built as follows (see the table below for allowed parameters):
```sh
sudo distrobuilder build-lxd devuan.yaml -o image.release=ascii -o image.architecture=amd64
lxc image import lxd.tar.xz rootfs.squashfs --alias spack/devuan/ascii/amd64
```


Possible parameter choices:

| Distribution | Releases | Instruction set architectures |
| -- | -- | -- |
| Alpine Linux | `3.12` `3.13` `3.14` `edge` | `x86_64` |
| CentOS | `7` `8` | `x86_64` |
| Devuan | `ascii` `beowfulf` `chimaera` `ceres` | `amd64` |

The Devuan and Alpine image configuration files might also work on `i386` and `aarch64`.
