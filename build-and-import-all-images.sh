#!/bin/sh
# Copyright 2021-2022 Institut National de Recherche en Informatique et en Automatique (Inria)

# This script creates Docker and LXD images with all Melissa dependencies
# installed for sensitivity analysis ("Melissa" or "Melissa SA" for short) and
# data assimilation (Melissa DA).
#
# The choice of distributions and releases depends on the target platforms of
# the Melissa developers, e.g., there are CentOS 8 images because the
# supercomputer JUWELS runs on this operating system.

# Reading hints:
# * Docker image _tag_ = LXD image _alias_
# * ISA = instruction set architecture
# * The instruction set architecture name "amd64" is used in favor of the
#   synonym "x86_64".

set -e
set -u


get_releases() {
	local distribution="${1:?}"

	if [ "$distribution" = 'alpine' ]; then
		echo 'edge'
	elif [ "$distribution" = 'centos' ]; then
		echo '7'
	elif [ "$distribution" = 'debian' ]; then
		echo '9'
		echo '10'
		echo '11'
	elif [ "$distribution" = 'devuan' ]; then
		echo 'ascii'
		echo 'beowulf'
		echo 'chimaera'
	elif [ "$distribution" = 'rockylinux' ]; then
		echo '8'
	elif [ "$distribution" = 'ubuntu' ]; then
		echo '18.04'
		echo '20.04'
	else
		>/dev/stderr "unknown distribution '$distribution'"
		exit 1
	fi
}


is_in_list() {
	local item="${1:?}"
	local list="${2:?}"

	echo "$list" | fgrep --quiet --word-regexp "$item"
}


# get compatible instruction set architecture (ISA) targets
get_compatible_architectures() {
	local distribution="${1:?}"
	local release="${2:?}"
	local machine="$(uname --machine)"

	if [ "$machine" = 'aarch64' ]; then
		if $(is_in_list "$distribution" 'alpine debian devuan'); then
			echo "$machine"
			echo 'armhf'
		fi
		if [ "$distribution" = 'rockylinux' ] && [ "$release" = '8' ]; then
			echo "$machine"
		fi
		return
	fi
	if [ "$machine" = 'x86_64' ]; then
		if $(is_in_list "$distribution" 'alpine debian devuan'); then
			echo 'i386'
		fi
		echo 'amd64'
		return
	fi

	>/dev/stderr echo "unknown machine $machine"
	exit 1
}


pdaf_tarball="${1:?Path to PDAF 1.16 tarball needed}"
cwd="$(pwd -P)"
directory_name="$(basename "$cwd")"

if [ "$directory_name" != 'melissa-ci' ]; then
	>/dev/stderr echo 'this script must be run in the melissa-ci directory'
	exit 1
fi

if [ ! -f "$pdaf_tarball" ]; then
	>/dev/stderr "'$pdaf_tarball' is not a regular file or does not exist"
	exit 1
fi

tmpdir="$(mktemp --directory --tmpdir -- melissa-ci.XXXXXXXX)"

echo "Created scratch directory '$tmpdir'"


# Build Docker images
if [ -n "$(which docker)" ]
then
    mkdir -- "$tmpdir/docker"
    cd -- "$tmpdir/docker"

    for distribution in $(ls -1 "$cwd/docker"); do
        dockerfile_source_dir="$cwd/docker/$distribution/"
        dockerfile_binary_dir="$tmpdir/docker/$distribution/"

        mkdir -- "$dockerfile_binary_dir"
        cp -- "$dockerfile_source_dir/Dockerfile" "$dockerfile_binary_dir"

        for release in $(get_releases "$distribution"); do
            tag="datamove/$distribution/$release"

            echo "Building Docker image '$tag'"
            cd -- "$dockerfile_binary_dir"
            tar -zxf "$pdaf_tarball"
            docker build \
                --build-arg "release=$release" \
                --tag "$tag" \
                -- .
        done
    done
fi


mkdir -- "$tmpdir/lxd"

for config_path in $(find "$cwd/lxd" -maxdepth 1 -type f -name '*.yaml'); do
	distribution="$(basename "$config_path" .yaml)"

	for release in $(get_releases "$distribution"); do
		for arch in $(get_compatible_architectures "$distribution" "$release")
		do
			alias="datamove/$distribution/$release/$arch"
			working_directory="$tmpdir/lxd/$distribution/$release/$arch"

			mkdir --parents -- "$working_directory"
			cd -- "$working_directory"

			echo "Building LXD image '$alias'"
			bash "$cwd/build-and-import-lxd-image.sh" \
				"$config_path" "$release" "$arch" "$alias" "$pdaf_tarball"
		done
	done
done

rm -r -- "$tmpdir"
