# Authentication info
AuthType=auth/munge
#AuthInfo=/var/run/munge/munge.socket.2
# slurmdbd info
DebugLevel=4
#DbdAddr=localhost
DbdHost=$HOSTPREFIX-0
#DbdPort=6819
#LogFile=
#MessageTimeout=300
PidFile=/var/run/slurm/slurmdbd.pid
#PluginDir=
#PrivateData=accounts,users,usage,jobs
PurgeEventAfter=1month
PurgeJobAfter=1month
PurgeResvAfter=1month
PurgeStepAfter=1month
PurgeSuspendAfter=1month
PurgeTXNAfter=1month
PurgeUsageAfter=1month
SlurmUser=root
#TrackWCKey=yes
#
# Database info
StorageType=accounting_storage/mysql
#StorageHost=localhost
#StoragePort=1234
StoragePass=slurm-db-password
StorageUser=slurm
StorageLoc=slurm_acct_db
