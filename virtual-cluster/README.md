# LXD Virtual Cluster

This directory contains LXD images and script for creating an OAR or Slurm cluster comprised of multiple LXD containers.


## Requirements

* LXD 3.0 or newer (old releases may work as well but this was not tested)
* [distrobuilder](https://github.com/lxc/distrobuilder)

LXD should be available in the package manager, for the distrobuilder installation, please visit its GitHub website and consult the README for up to date instructions. (As of Februrary 2021, the distrobuilder README explains installation on Debian- and ArchLinux-based systems.)


## Setup

If you never ran LXD before, call `lxd init` as superuser. Throghout this section, the text assumes that you use the default settings. In particular,
* the default storage pool is called `default` and
* containers can communicate with each other.

LXD manages storage with so-called _pools_. Inside a pool are individual _volumes_ where a volume can be thought of as the LXD equivalent of a computer's hard disk drive. On a cluster, the individual nodes managed by a batch scheduler usually share access to the user's home directory. To achieve a similar effect in LXD, several containers are made to work with the same volume. Create a shared volume called `shared` in the `default` storage pool:
```sh
lxc storage volume create default shared
```

This should yield the following Error message:
```
$ lxc something
If this is your first time running LXD on this machine, you should also run: lxd init
To start your first container, try: lxc launch ubuntu:18.04
Error: Get http://unix.socket/1.0: dial unix /var/snap/lxd/common/lxd/unix.socket: connect: permission denied
```
Cause:
* The command is not executed by the superuser and the user is not a member of the group `lxd`.
Fix:
* Execute as superuser with `sudo`
* Make user member of the `lxd` group: `sudo adduser <username> lxd`

*All* OAR and Slurm containers will access the same storage. Keep this in mind when having both a Slurm and an OAR virtual cluster at the same time.


### Building the Container Images

Next, locate your distrobuilder installation (usually `$HOME/go/bin`) and build the LXD image. distrobuilder will create two files in the current working directory called `lxd.tar.gz` and `rootfs.squashfs`; it will overwrite existing files. Keep this in mind when building OAR and Slurm images.

For OAR on Debian, run
```sh
sudo env PATH="$PATH:$HOME/go/bin" \
    distrobuilder build-lxd debian.yaml \
        -o image.release=buster \
        -o image.architecture=x86_64 \
        -o image.variant=virtual-cluster
```
Regarding the `image.release` option, possible Debian releases are Debian 9 (codename _stretch_) and Debian 10 (buster); possible image architectures are `i386` and `x86_64`. For Slurm on CentOS, execute
```sh
sudo env PATH="$PATH:$HOME/go/bin" \
    distrobuilder build-lxd centos.yaml \
        -o image.release=8 \
        -o image.architecture=x86_64 \
        -o image.variant=virtual-cluster \
        -o source.variant=boot
```
When distrobuilder is finished, import the LXD image; the alias is just a proposal and you need to remember the alias:
```sh
lxc image import \
    --alias 'virtual-cluster/debian/10/amd64' \
    -- lxd.tar.xz rootfs.squashfs
```


### Launching a Virtual Cluster

This step requires that you built an LXD image, imported it, and remember its alias. Run one of the following commands in the `melissa-ci/virtual-cluster` directory:
```sh
python3 launch-virtual-cluster.py --help
```
shows the available command line parameters of this script.

**This script stops and deletes existing virtual clusters!** The OAR script destroys only OAR clusters; the Slurm script destroys only Slurm clusters. One container in every virtual cluster will only schedule jobs but never run them so there must always be at least two containers.

Launch a virtual cluster consisting of 12 containers (one server, eleven compute nodes):
```sh
python3 launch-virtual-cluster.py oar 12 'virtual-cluster/debian/10/amd64'
python3 launch-virtual-cluster.py slurm 12 'virtual-cluster/centos/8/amd64'
```

To run an application, log into master node:
```sh
lxc exec slurm-0 -- sudo --login --user john
```
Inside the container, compile your MPI application and start it with
```sh
oarsub -l cores=3 -- 'mpirun -machinefile $OAR_FILE_NODES -n 3 -- mpi-application'
```
or
```sh
srun -n 3 -- mpi-application
```


## Developer Hints

The Python launcher script can be type checked with [mypy](http://mypy-lang.org/):
```sh
python3 -m mypy launch-virtual-cluster.py
```

### Slurm

If you need to modify or port the Slurm virtual cluster, then follow the instructions below because updating the (working) configuration in its entirety and then attempting to solve the problems that occur one by one will not work because on every virtual node, at least three daemons must successfully interact and in-between nodes, the communication must work as well.

* Try to make Slurm run on one node with the default configuration provided by the distribution and without job accounting (job accounting involves a fourth daemon, `slurmdbd`).
* If this does not work, then try the items below:
  * Look at the slurmd log files (note: CentOS disables slurmd logging in `slurm.conf` by default).
  * Read the [_Slurm Troubleshooting Guide_](https://slurm.schedmd.com/troubleshoot.html).
  * Run the daemons on each node with the `-D` parameter and add zero or more verbose flags, e.g., `slurmctld -D -vv`, `slurmd -D`.
* Customize the configuration, e.g, modify the scheduler type.
* Try to make Slurm run on two nodes without job accounting, then on as many nodes as desired.
* Enable the Slurm job accounting.
* Check if Slurm is running as superuser (CentOS does this by default) and decide if this poses an acceptable risk.


### Setting up a virtual Slurm cluster for melissa-da ci:

* make sure `~/PDAF-D_V1.16.tar.gz` exists
* build the virtual cluster:
```sh
python3 launch-virtual-cluster.py slurm 3 virtual-cluster/rockylinux/8/amd64 -v --prefix test --install-gitlab-runner
```
* then register and start the runner as described in the output log. Set `virtual-cluster-slurm` as tag and activate it on gitlab.inria.fr 
